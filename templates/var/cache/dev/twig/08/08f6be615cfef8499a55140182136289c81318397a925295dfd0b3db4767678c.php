<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* professeur/index.html.twig */
class __TwigTemplate_ccc1ea1db1559f8a495f9e51874eb3fa474e24cb5f1423098f077458da81e086 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "professeur/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "professeur/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "EDT - Professeurs";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<style>
  .header {
    display: flex;
    align-items: center;
  }

  .header>h1 {
    margin-right: 1.5rem;
  }

  button {
    z-index: 1;
    position: relative;
    font-size: inherit;
    font-family: inherit;
    color: white;
    padding: 0.5em 1em;
    outline: none;
    border: none;
    background-color: hsl(236, 32%, 26%);
  }

  button::before {
    content: '';
    z-index: -1;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: #fc2f70;
    transform-origin: center right;
    transform: scaleX(0);
    transition: transform 0.25s ease-in-out;
  }

  button:hover {
    cursor: pointer;
  }

  button:hover::before {
    transform-origin: center left;
    transform: scaleX(1);
  }
</style>
<div class=\"header\">
  <h1>Liste des professeurs</h1>
  <a href=\"";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("professeurs.create");
        echo "\">
    <button>Créer</button>
  </a>
</div>

<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>Prénom</th>
      <th>Nom</th>
      <th>Email</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    ";
        // line 66
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["professeurs"]) || array_key_exists("professeurs", $context) ? $context["professeurs"] : (function () { throw new RuntimeError('Variable "professeurs" does not exist.', 66, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["professeur"]) {
            // line 67
            echo "    <tr>
      <td>";
            // line 68
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["professeur"], "id", [], "any", false, false, false, 68), "html", null, true);
            echo "</td>
      <td>";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["professeur"], "prenom", [], "any", false, false, false, 69), "html", null, true);
            echo "</td>
      <td>";
            // line 70
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["professeur"], "nom", [], "any", false, false, false, 70), "html", null, true);
            echo "</td>
      <td>";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["professeur"], "email", [], "any", false, false, false, 71), "html", null, true);
            echo "</td>
      <td>
        <a href=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("professeurs.edit", ["id" => twig_get_attribute($this->env, $this->source, $context["professeur"], "id", [], "any", false, false, false, 73)]), "html", null, true);
            echo "\">Éditer</a>
        <a href=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("professeurs.delete", ["id" => twig_get_attribute($this->env, $this->source, $context["professeur"], "id", [], "any", false, false, false, 74)]), "html", null, true);
            echo "\">Supprimer</a>
      </td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['professeur'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "  </tbody>
</table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "professeur/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 78,  168 => 74,  164 => 73,  159 => 71,  155 => 70,  151 => 69,  147 => 68,  144 => 67,  140 => 66,  121 => 50,  72 => 3,  65 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %} {% block title %}EDT - Professeurs{% endblock %}
{% block body %}
<style>
  .header {
    display: flex;
    align-items: center;
  }

  .header>h1 {
    margin-right: 1.5rem;
  }

  button {
    z-index: 1;
    position: relative;
    font-size: inherit;
    font-family: inherit;
    color: white;
    padding: 0.5em 1em;
    outline: none;
    border: none;
    background-color: hsl(236, 32%, 26%);
  }

  button::before {
    content: '';
    z-index: -1;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: #fc2f70;
    transform-origin: center right;
    transform: scaleX(0);
    transition: transform 0.25s ease-in-out;
  }

  button:hover {
    cursor: pointer;
  }

  button:hover::before {
    transform-origin: center left;
    transform: scaleX(1);
  }
</style>
<div class=\"header\">
  <h1>Liste des professeurs</h1>
  <a href=\"{{ path('professeurs.create') }}\">
    <button>Créer</button>
  </a>
</div>

<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>Prénom</th>
      <th>Nom</th>
      <th>Email</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    {% for professeur in professeurs %}
    <tr>
      <td>{{ professeur.id }}</td>
      <td>{{ professeur.prenom }}</td>
      <td>{{ professeur.nom }}</td>
      <td>{{ professeur.email }}</td>
      <td>
        <a href=\"{{ path('professeurs.edit', { id: professeur.id }) }}\">Éditer</a>
        <a href=\"{{ path('professeurs.delete', { id: professeur.id }) }}\">Supprimer</a>
      </td>
    </tr>
    {% endfor %}
  </tbody>
</table>
{% endblock %}", "professeur/index.html.twig", "/home/plaborderie/IUT-php/edt/templates/professeur/index.html.twig");
    }
}
