# API du projet Symfony/VueJS (Théo Malboeuf et Paul Laborderie)

## Note

Le code VueJS est dans le dossier front, le code Symfony dans api.
Cependant, le front a été build dans le dossier `public` de Symfony, et est donc accessible depuis localhost:8000.
Ainsi il n'est pas nécessaire de lancer le server node.

## Fonctionnalités développées :

- API :
  - Routes d'API demandées
  - Interface d'administration fonctionnelle
  - Date de fin de cours forcément supérieure à date de début
  - Impossible d'avoir plusieurs cours en même temps dans la même salle (Validateur sur l'entité Cours)
  - Date de début de cours ne peut pas être dans le passé
  - Liste de cours triés par heure
  - Dans un cours, le professeur doit forcément enseigner la matière sélectionnée (Validateur callback)
  - Export des cours sous fichier .ics pour enregistrer dans son agenda
- Interface VueJS
  - Utilisation de vue-router pour naviguer entre l'edt et note ton prof
  - Utilisation de [Buefy](https://buefy.org/) pour le style (intégration VueJS de [Bulma](https://bulma.io/))
  - Affichage de notifications en cas d'erreurs
  - Note ton prof fonctionnel
  - Affichage de la liste des cours de la journée
  - Boutons pour changer de journée (précédent/suivant)
  - Interface responsive (adaptée pour téléphones et ordinateurs)
  - Emplois du temps plus détaillé avec jour, semaine et mois
  - Système d'authentification

## Idées de développement supplémentaire

- Sélection date heure dans EasyAdmin plus simple (bootstrap ?)
- Intégration de Note Ton Prof en cliquant sur le prof depuis un cours
- Affichage de la note moyenne d'un prof dans la liste des cours
- Barre de recherche et divers filtres pour les prochains cours
- Migration vers une PWA pour installer l'app sur mobile (ou desktop si sur Google Chrome)
