<?php

namespace App\Controller\Api;

use App\Entity\Avis;
use App\Entity\Cours;
use App\Entity\Etudiant;
use App\Entity\Professeur;
use App\Entity\Salle;
use App\Form\AvisType;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api")
 */
class ProfesseurController extends AbstractController
{
    /**
     * @Route("/professeurs", name="api_get_professeurs", methods={"GET"})
     */
    public function getProfesseurs(EntityManagerInterface $em)
    {
        $professeurs = $em->getRepository(Professeur::class)->findAll();

        return $this->json($professeurs, 200, [], ['ignored_attributes' => ['professeurs', 'professeur', 'cours']]);
    }

    /**
     * @Route("/professeur/{id}", name="api_get_professeur", methods={"GET"})
     */
    public function getProfesseur(Professeur $professeur)
    {
        return $this->json($professeur, 200, [], ['ignored_attributes' => ['professeurs', 'professeur', 'cours']]);
    }

    /**
     * @Route("/professeur/{id}/avis", name="api_get_professeur_avis", methods={"GET"})
     */
    public function getAvis(Professeur $professeur)
    {
        $user = $this->getUser();
        return $this->json(array_map(function (Avis $avis) use ($user) {
            return $avis->toArray();
        }, $professeur->getAvis()->toArray()));
    }

    /**
     * @Route("/professeur/{id}/avis", name="api_put_professeur_avis", methods={"PUT"})
     */
    public function putAvis(Professeur $professeur, EntityManagerInterface $em, Request $req)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $data = json_decode($req->getContent(), true);
        $data['professeur'] = $professeur->getId();
        $data['emailEtudiant'] = $this->getUser()->getUsername();
        $form = $this->createForm(AvisType::class, new Avis(), ['csrf_protection' => false]);
        $form->submit($data);

        if (!$form->isValid()) {
            return $this->json($this->getFormErrors($form), 400);
        }

        $avis = $form->getData();
        $em->persist($avis);
        $em->flush();
        return $this->json($avis->toArray(), 201);
    }

    /**
     * @Route("/avis/{id}", name="api_delete_avis", methods={"DELETE"})
     */
    public function deleteAvis(EntityManagerInterface $em, Avis $avis)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->getUser()->getUsername() !== $avis->getEmailEtudiant()) {
            return $this->json(['message' => "Vous ne pouvez pas supprimer l'avis d'un autre utilisateur."], 403);
        }
        $em->remove($avis);
        $em->flush();
        return $this->json(null, 204);
    }

    /**
     * @Route("/cours", name="api_get_cours", methods={"GET"})
     */
    public function getCours(EntityManagerInterface $em)
    {
        $cours = $em->getRepository(Cours::class)->findBy([], ['dateHeureDebut' => 'ASC']);
        return $this->json($cours, 200, [], ["ignored_attributes" => ["cours", "professeurs", "matieres", "avis"]]);
    }

    /**
     * @Route("/cours/download", name="api_download_cours", methods={"GET"})
     */
    public function downloadCours(EntityManagerInterface $em)
    {
        $cours = $em->getRepository(Cours::class)->findAll();
        $vCalendar = new \Eluceo\iCal\Component\Calendar("EDT_IUT");
        foreach ($cours as $unCours) {
            $matiere = $unCours->getMatiere();
            $vEvent = new \Eluceo\iCal\Component\Event();
            $nomProf = $unCours->getProfesseur()->getPrenom() . ' ' . $unCours->getProfesseur()->getNom();
            $vEvent
                ->setDtStart($unCours->getDateHeureDebut())
                ->setDtEnd($unCours->getDateHeureFin())
                ->setSummary($matiere->getReference() . ' - ' . $matiere->getTitre() . '(' . $unCours->getType() . ')')
                ->setLocation('IUT de Bayonne et du Pays Basque')
                ->setDescription($nomProf . ' - Salle ' . $unCours->getSalle()->getNumero());
            $vCalendar->addComponent($vEvent);
        }

        return new Response(
            $vCalendar->render(),
            200,
            array(
                'Content-Type' => 'text/calendar; charset=utf-8',
                'Content-Disposition' => 'attachment; filename="edt.ics"',
            )
        );
    }

    /**
     * @Route("/salles", name="api_get_salles", methods={"GET"})
     */
    public function getSalles(EntityManagerInterface $em)
    {
        $salles = $em->getRepository(Salle::class)->findAll();

        return $this->json($salles, 200, [], ["groups" => ["liste_salles"]]);
    }

    /**
     * @Route("/me", name="api_me", methods={"GET"})
     */
    public function me()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->json(["email" => $this->getUser()->getEmail()], 200);
    }

    /**
     * @Route("/login", name="api_login")
     */
    public function login(Request $req)
    {
        $user = $this->getUser();

        return $this->json(['username' => $user->getUsername(), 'roles' => $user->getRoles()]);
    }

    /**
     * @Route("/signup", name="api_signup")
     */
    public function signup(Request $req, UserPasswordEncoderInterface $passwordEncoder)
    {
        $etudiant = new Etudiant();
        $data = json_decode($req->getContent(), true);
        $form = $this->createForm(RegistrationFormType::class, $etudiant, ['csrf_protection' => false]);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($etudiant, $data['plainPassword']);
            $etudiant->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($etudiant);
            $em->flush();
            return $this->json(['username' => $etudiant->getUsername()], 200);
        }

        return $this->json($this->getFormErrors($form), 400);
    }

    private function getFormErrors(Form $form)
    {
        $errors = array();

        // Global
        foreach ($form->getErrors() as $error) {
            $errors[$form->getName()][] = $error->getMessage();
        }

        //Fields
        foreach ($form as $child) {
            if (!$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$child->getName()][] = $error->getMessage();
                }
            }
        }

        return $errors;
    }
}
