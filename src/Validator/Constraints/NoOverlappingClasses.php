<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 *@Annotation
 */
class NoOverlappingClasses extends Constraint
{
  public $message = "Il n'y peut y avoir qu'un seul cours dans la même salle en même temps.";

  public function getTargets()
  {
    return self::CLASS_CONSTRAINT;
  }
}
