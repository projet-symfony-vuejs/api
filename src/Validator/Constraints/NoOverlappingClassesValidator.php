<?php

namespace App\Validator\Constraints;

use App\Validator\Constraints\NoOverlappingClasses;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NoOverlappingClassesValidator extends ConstraintValidator
{

  private $em;

  public function __construct(EntityManagerInterface $em)
  {
    $this->em = $em;
  }
  public function validate($value, Constraint $constraint)
  {
    if (!$constraint instanceof NoOverlappingClasses) {
      throw new UnexpectedTypeException($constraint, NoOverlappingClasses::class);
    }

    $conn = $this->em->getConnection();
    $sql = "SELECT COUNT(*)
    FROM cours c JOIN salle s ON salle_id = s.id
    WHERE :start <= c.date_heure_fin AND :end >= c.date_heure_debut AND s.id = :salle
    ";
    $statement = $conn->prepare($sql);
    $statement->bindValue("start", $value->getDateHeureDebut()->format('Y-m-d H:i:s'));
    $statement->bindValue("end", $value->getDateHeureFin()->format('Y-m-d H:i:s'));
    $statement->bindValue("salle", $value->getSalle()->getId());
    $statement->execute();

    $res = (int) $statement->fetchColumn();

    if ($res > 0) {
      $this->context->buildViolation($constraint->message)
        ->atPath("dateHeureDebut")
        ->addViolation();
    }
  }
}
