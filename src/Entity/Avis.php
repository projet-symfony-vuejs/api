<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvisRepository")
 * @UniqueEntity(
 *   fields={"emailEtudiant", "professeur"},
 *   errorPath="emailEtudiant",
 *   message="Cet étudiant a déjà noté ce professeur."
 * )
 */
class Avis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 0, max = 5)
     */
    private $note;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $commentaires;

    /**
     * @ORM\Column(type="string")
     * @Assert\Email()
     */
    private $emailEtudiant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Professeur", inversedBy="avis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $professeur;

    public function __toString()
    {
        return sprintf("%s (%s/5)", $this->emailEtudiant, $this->note);
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'note' => $this->getNote(),
            'commentaires' => $this->getCommentaires(),
            'emailEtudiant' => $this->getEmailEtudiant(),
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getCommentaires(): ?string
    {
        return $this->commentaires;
    }

    public function setCommentaires(string $commentaires): self
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    public function getEmailEtudiant(): ?string
    {
        return $this->emailEtudiant;
    }

    public function setEmailEtudiant(string $emailEtudiant): self
    {
        $this->emailEtudiant = $emailEtudiant;

        return $this;
    }

    public function getProfesseur(): ?Professeur
    {
        return $this->professeur;
    }

    public function setProfesseur(?Professeur $professeur): self
    {
        $this->professeur = $professeur;

        return $this;
    }
}
